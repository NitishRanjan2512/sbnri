package com.nitish.nitishranjansbnri.db;

import com.nitish.nitishranjansbnri.model.RepoModel;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;


public class LocalRealmDB {

    private final Realm realm;

    public LocalRealmDB(Realm realm) {
        this.realm = realm;
    }

    public RealmResults<RepoModel> getRepoListDb() {
        return realm.where(RepoModel.class).findAllAsync().sort("id", Sort.ASCENDING);
    }

    public void saveRepos(final List<RepoModel> products) {
        realm.executeTransactionAsync(realm -> realm.copyToRealmOrUpdate(products));
    }
}
