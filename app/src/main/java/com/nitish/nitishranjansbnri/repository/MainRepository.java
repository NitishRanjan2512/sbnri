package com.nitish.nitishranjansbnri.repository;


import com.nitish.nitishranjansbnri.db.LocalRealmDB;
import com.nitish.nitishranjansbnri.model.RepoModel;
import com.nitish.nitishranjansbnri.network.APIService;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import io.realm.RealmResults;

@Singleton
public class MainRepository {

    private final LocalRealmDB db;
    private final APIService apiService;

    @Inject
    public MainRepository(LocalRealmDB db, APIService apiService) {
        this.db = db;
        this.apiService = apiService;
    }

    public RealmResults<RepoModel> getRepoList() {
        return db.getRepoListDb();
    }



    public Observable<List<RepoModel>> fetchAndPersistRepos(int page) {
        return apiService.getRepoList(page, 10)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(new Function<List<RepoModel>, List<RepoModel>>() {
                    @Override
                    public List<RepoModel> apply(@NonNull List<RepoModel> productListResponse) throws Exception {
                        return productListResponse;
                    }
                })
                .doOnNext(new Consumer<List<RepoModel>>() {
                    @Override
                    public void accept(@NonNull List<RepoModel> repoModelList) throws Exception {
                        db.saveRepos(repoModelList);
                    }
                });
    }
}
