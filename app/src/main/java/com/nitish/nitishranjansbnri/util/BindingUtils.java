package com.nitish.nitishranjansbnri.util;

import android.view.View;

import androidx.databinding.BindingAdapter;


public class BindingUtils {

    @BindingAdapter("bind:visible")
    public static void bindVisible(View view, boolean visible) {
        view.setVisibility(visible ? View.VISIBLE : View.GONE);
    }
}
