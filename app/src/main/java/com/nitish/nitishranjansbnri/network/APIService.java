package com.nitish.nitishranjansbnri.network;

import com.nitish.nitishranjansbnri.model.RepoModel;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface APIService {

    @GET("orgs/octokit/repos")
    Observable<List<RepoModel>> getRepoList(@Query("page") int page, @Query("per_page") int parPage);

}
