package com.nitish.nitishranjansbnri;


import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nitish.nitishranjansbnri.databinding.ActivityMainBinding;
import com.nitish.nitishranjansbnri.di.ActivityComponent;
import com.nitish.nitishranjansbnri.model.RepoModel;
import com.nitish.nitishranjansbnri.repo.RepoListAdapter;
import com.nitish.nitishranjansbnri.repo.RepoListViewModel;

import javax.inject.Inject;

import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.RealmResults;

public class MainActivity  extends AppCompatActivity {

    @Inject
    RepoListViewModel viewModel;
    private RepoListAdapter adapter;
    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActivityComponent.component(this).inject(this);

        Context context = this;
        ActivityMainBinding binding= DataBindingUtil.setContentView(this, R.layout.activity_main);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

         viewModel.getRepo();
        binding.setVm(viewModel);
        layoutManager=new LinearLayoutManager(context);
        binding.recyclerView.setLayoutManager(layoutManager);
        adapter=new RepoListAdapter(viewModel.getRepos());
        binding.recyclerView.setAdapter(adapter);

        binding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (!viewModel.hasMore()) {
                    return;
                }
                int currentPage = (layoutManager.findLastCompletelyVisibleItemPosition()+1)/10;
                if (currentPage >= viewModel.getPage()) {
                    viewModel.loadForPage(currentPage + 1);
                }
            }
        });

        viewModel.loadForPage(1);
        viewModel.getRepos().addChangeListener(changeListener);

    }



    private final OrderedRealmCollectionChangeListener<RealmResults<RepoModel>> changeListener =
            new OrderedRealmCollectionChangeListener<RealmResults<RepoModel>>() {
                @Override
                public void onChange(RealmResults<RepoModel> collection, OrderedCollectionChangeSet changeSet) {
                    // `null`  means the async query returns the first time.
                    if (changeSet == null) {
                        adapter.notifyDataSetChanged();
                        return;
                    }
                    // For deletions, the adapter has to be notified in reverse order.
                    OrderedCollectionChangeSet.Range[] deletions = changeSet.getDeletionRanges();
                    for (int i = deletions.length - 1; i >= 0; i--) {
                        OrderedCollectionChangeSet.Range range = deletions[i];
                        adapter.notifyItemRangeRemoved(range.startIndex, range.length);
                    }

                    OrderedCollectionChangeSet.Range[] insertions = changeSet.getInsertionRanges();
                    for (OrderedCollectionChangeSet.Range range : insertions) {
                        adapter.notifyItemRangeInserted(range.startIndex, range.length);
                    }

                    OrderedCollectionChangeSet.Range[] modifications = changeSet.getChangeRanges();
                    for (OrderedCollectionChangeSet.Range range : modifications) {
                        adapter.notifyItemRangeChanged(range.startIndex, range.length);
                    }
                }
            };


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}

