package com.nitish.nitishranjansbnri.repo;


import androidx.lifecycle.ViewModel;

import com.nitish.nitishranjansbnri.model.RepoModel;

public class RepoItemViewModel extends ViewModel {

    private RepoModel repo;

    public RepoModel getProduct() {
        return repo;
    }

    public void setProduct(RepoModel product) {
        this.repo = product;
    }
}
