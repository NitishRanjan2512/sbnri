package com.nitish.nitishranjansbnri.repo;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.nitish.nitishranjansbnri.R;
import com.nitish.nitishranjansbnri.databinding.ItemRepoBinding;
import com.nitish.nitishranjansbnri.model.RepoModel;

import io.realm.RealmResults;

public class RepoListAdapter extends RecyclerView.Adapter<RepoListAdapter.ItemViewHolder> {

    private final RealmResults<RepoModel> dataList;

    public RepoListAdapter(RealmResults<RepoModel> dataList) {
        this.dataList = dataList;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemRepoBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_repo, parent, false);
        return new ItemViewHolder(binding, new RepoItemViewModel());
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        holder.bind(dataList.get(position));
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }



    class ItemViewHolder extends RecyclerView.ViewHolder {

        private final RepoItemViewModel viewModel;
        private final ItemRepoBinding binding;

        public ItemViewHolder(ItemRepoBinding binding, RepoItemViewModel viewModel) {
            super(binding.getRoot());
            this.viewModel = viewModel;
            this.binding = binding;
        }

        public void bind(RepoModel repoModel) {
            viewModel.setProduct(repoModel);
            binding.setVm(viewModel);
            binding.executePendingBindings();
        }
    }

}
