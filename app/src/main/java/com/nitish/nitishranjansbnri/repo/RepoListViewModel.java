package com.nitish.nitishranjansbnri.repo;

import androidx.databinding.ObservableField;
import androidx.lifecycle.ViewModel;

import com.nitish.nitishranjansbnri.model.RepoModel;
import com.nitish.nitishranjansbnri.repository.MainRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.realm.RealmResults;


public class RepoListViewModel extends ViewModel {

    private MainRepository mainRepository;
    private RealmResults<RepoModel> repos;

    private boolean hasMore=true;
    private final ObservableField<Integer> page;
    private final ObservableField<Boolean> showError;
    private final ObservableField<Boolean> isLoading;

    @Inject
    public RepoListViewModel(MainRepository mainRepository) {
        this.mainRepository = mainRepository;
        this.page=new ObservableField<>(1);
        this.showError = new ObservableField<>(false);
        this.isLoading = new ObservableField<>(false);
    }


    public void getRepo(){
        this.repos = mainRepository.getRepoList();
    }

    public ObservableField<Boolean> getShowError() {
        return showError;
    }

    public ObservableField<Boolean> getIsLoading() {
        return isLoading;
    }

    public RealmResults<RepoModel> getRepos() {
        return repos;
    }

    public void loadForPage(final int requestedPage) {
        if (!isLoading.get()) {
            isLoading.set(true);
            showError.set(false);
            mainRepository.fetchAndPersistRepos(requestedPage).subscribe(new Observer<List<RepoModel>>() {
                @Override
                public void onSubscribe(@NonNull Disposable d) {

                }

                @Override
                public void onNext(@NonNull List<RepoModel> products) {
                    hasMore = products.size() == 10;
                }

                @Override
                public void onError(@NonNull Throwable e) {
                    isLoading.set(false);
                    if (repos.size() == 0) {
                        showError.set(true);
                    }
                    e.printStackTrace();
                }

                @Override
                public void onComplete() {
                    isLoading.set(false);
                    page.set(requestedPage);
                }
            });
        }
    }

    public int getPage() {
        return page.get();
    }

    public boolean hasMore() {
        return hasMore;
    }

}
