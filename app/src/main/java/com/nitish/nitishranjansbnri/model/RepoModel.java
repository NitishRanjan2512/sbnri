package com.nitish.nitishranjansbnri.model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class RepoModel extends RealmObject {

    @PrimaryKey
    private long id;
    private String name;
    private String description;
    @SerializedName("open_issues_count")
    private String openIssueCount;
    private PermissionModel permissions;
    private LicenseModel license;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof RepoModel) {
            return ((RepoModel)obj).getId() == id;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return (int) (id % Integer.MAX_VALUE);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOpenIssueCount() {
        return openIssueCount;
    }

    public void setOpenIssueCount(String openIssueCount) {
        this.openIssueCount = openIssueCount;
    }

    public LicenseModel getLicense() {
        return license;
    }

    public void setLicense(LicenseModel license) {
        this.license = license;
    }

    public PermissionModel getPermissions() {
        return permissions;
    }

    public void setPermissions(PermissionModel permissions) {
        this.permissions = permissions;
    }
}
