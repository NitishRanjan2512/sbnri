package com.nitish.nitishranjansbnri.model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class PermissionModel extends RealmObject {

	@SerializedName("pull")
	private boolean pull;

	@SerializedName("admin")
	private boolean admin;

	@SerializedName("push")
	private boolean push;

	public void setPull(boolean pull){
		this.pull = pull;
	}

	public boolean isPull(){
		return pull;
	}

	public void setAdmin(boolean admin){
		this.admin = admin;
	}

	public boolean isAdmin(){
		return admin;
	}

	public void setPush(boolean push){
		this.push = push;
	}

	public boolean isPush(){
		return push;
	}

	@Override
 	public String toString(){
		return 
			"PermissionModel{" + 
			"pull = '" + pull + '\'' + 
			",admin = '" + admin + '\'' + 
			",push = '" + push + '\'' + 
			"}";
		}
}