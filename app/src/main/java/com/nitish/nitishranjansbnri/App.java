package com.nitish.nitishranjansbnri;


import android.app.Application;
import android.content.Context;
import com.nitish.nitishranjansbnri.di.AppComponent;

import io.realm.Realm;
import okhttp3.OkHttpClient;

public class App extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        appComponent = AppComponent.component(this);
        appComponent.inject(this);

    }


    public static AppComponent component(Context context) {
        return ((App) context.getApplicationContext()).appComponent;
    }

}
