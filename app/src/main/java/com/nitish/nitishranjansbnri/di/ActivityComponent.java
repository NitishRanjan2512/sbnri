package com.nitish.nitishranjansbnri.di;

import android.app.Activity;


import com.nitish.nitishranjansbnri.App;
import com.nitish.nitishranjansbnri.MainActivity;

import dagger.Subcomponent;

@PerActivity
@Subcomponent(modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(MainActivity activity);

    public static ActivityComponent component(Activity activity){
        return App.component(activity).plusActivityModule(new ActivityModule(activity));
    }
}
