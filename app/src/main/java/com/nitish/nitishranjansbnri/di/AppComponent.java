package com.nitish.nitishranjansbnri.di;

import android.app.Application;


import com.nitish.nitishranjansbnri.App;
import com.nitish.nitishranjansbnri.network.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {

    void inject(App app);

    ActivityComponent plusActivityModule(ActivityModule activityModule);

    public static AppComponent component(Application app){
        return DaggerAppComponent.builder().appModule(new AppModule(app)).networkModule(new NetworkModule()).persistenceModule(new PersistenceModule()).build();
    }

}
