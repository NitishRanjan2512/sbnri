package com.nitish.nitishranjansbnri.di;

import android.app.Application;
import android.content.Context;

import com.nitish.nitishranjansbnri.network.NetworkModule;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;



@Module(includes ={NetworkModule.class, PersistenceModule.class})
public class AppModule {

    private Application app;

    public AppModule(Application app) {
        this.app=app;
    }

    @Provides
    @Singleton
    Application providesApplication(){
        return app;
    }

    @Provides
    @Singleton
    @AppContext
    Context providesContext(){
        return app;
    }



}
