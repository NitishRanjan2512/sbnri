package com.nitish.nitishranjansbnri.di;

import com.nitish.nitishranjansbnri.db.LocalRealmDB;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import io.realm.RealmConfiguration;


@Module
public class PersistenceModule {

    @Provides
    public LocalRealmDB providesLocalRealmDB(Realm realm) {
        return new LocalRealmDB(realm);
    }

    @Provides
    public Realm providesRealm(RealmConfiguration realmConfiguration) {
        return Realm.getInstance(realmConfiguration);
    }

    @Provides
    @Singleton
    public RealmConfiguration providesRealmConfiguration() {
        RealmConfiguration.Builder builder = new RealmConfiguration.Builder();
        builder.deleteRealmIfMigrationNeeded();
        // builder.inMemory();
        return builder.build();
    }

}
